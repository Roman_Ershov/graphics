#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QVector>
#include <cmath>
#include <math.h>
#include <QDate>
#include <qpalette.h>
#include <QString>
#include <QPainter>
#include <QColor>
#define PI  3.14159265;

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

private slots:
    void on_Rasschet_clicked();

    void on_dateEdit_userDateChanged(const QDate &date);

    void on_Ish_ist_izl_clicked();

    void on_Sputniky_clicked();

    void on_Nach_pribl_clicked();

    void on_Pasch_ist_izl_clicked();

    void on_Sbros_clicked();

    void on_pushButton_clicked();



    void on_pushButton_2_clicked();

private:
    Ui::MainWidget *ui;
    QColor clr;
    QString S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14;
    QDate date;
    double c = 299792.458;//км/с
    //double PI = 3.14159265;
    struct IGASK
    {
        double x;
        double y;
        double z;

        // Почему бы не сделать это классом, в который включить метод перевода из геодезической СК,
        // а все вспомогательные функции инкапсулировать?
        // Со следующей структурой так же
    };

    struct GEOD
    {
        double shirota;
        double dolgota;
        double visota;
    };

    struct Vremya
    {
        int chas;
        int min;
        double sek;
    };

    struct Data
    {
        int year;
        int month;
        int day;
    };

    struct GRINVICH
    {
        double x;
        double y;
        double z;
    };
    double Vichislenie_Zv_vremya_Grinvich_v_Polnoch(Data data);
    double *Pol_t_P(double xp, double yp); //Матрица мгновенных значений полюса
    double *Zv_vremya_P(double vremya); //Матрица звездного времени на Гринвиче в определенную дату
    double *Pol_t_O(double xp, double yp); //Транспонированная матрица
    double *Zv_vremya_O(double vremya);//Транспонированная матрица

    /////// При переводе учитывается только дата - день, месяц, год
    /// Для повышения точности нужно учитывать также текущее время вплоть до долей секунды
    GRINVICH Perevod_is_IGASK_v_Grinvich(IGASK &koordinaty, Data data);//Произведение трех матриц
    IGASK Perevod_is_Grinvich_v_IGASK(GRINVICH &koordinaty, Data data);//Произведение трех матриц
    double radius_of_Earth(double phi);//Считаем радиус Земли
    GEOD perevod_s_IGASK(IGASK &koordinaty, Data data);//Перевод с геоцентрических в геодезические координаты
    IGASK perevod_s_GEOD(GEOD &koordinaty, Data data);//Перевод с геодезических в геоцентрические координаты
    //////////////////////////////////////////////////Разностно-дальномерный метод//////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    struct Sputnik
    {
        IGASK koord_sputnik_IGASK;//координаты спутника в ИГЭСК
        GEOD koord_sputnik_GEOD;//координаты спутника в геодезических координатах
    };
    struct Istochnik
    {
        GEOD koord_istochnik_GEOD;//координаты источника в геодезических координатах
        IGASK koord_istochnik_IGASK;//координаты источника в ИГЭСК
    };
    QVector<double> reshenie_sistemy(double Cx, double Cy, double Bx, double By, double Mx, double My, double Px, double Py);
    QVector<double> nach_priblizhenie(Sputnik sp1, Sputnik sp2, Sputnik sp3);//Центр тяжести треугольника
    double function(double* x, Sputnik sp1, Sputnik sp2, Sputnik sp3, double tau12, double tau13);
    void MHJ(int kk, double* x, Sputnik sp1, Sputnik sp2, Sputnik sp3, Istochnik &istochnik, double tau12, double tau13,Data data);
    double vremennye_zaderzhki(Istochnik istochnik, Sputnik sputnik1, Sputnik sputnik2);
    /////////////////////////////////////////////Линии Положения///////////////////////////////////////////////
    struct dann
    {
        QVector <double> x;
        QVector <double> y;
    };
    dann dann3,dann4,dann5,dann6,dann7,dann8;
    struct dannx
    {
        QVector <double> x;
        QVector <double> y;
        QVector <double> x1;
        QVector <double> y1;
    };
    dannx dannx1,dannx2;
    QVector<double> reshenie_sistemy2 (double Cx, double Cy, double Bx, double By, double Mx, double My, double Px, double Py);
    QVector<double> nach_priblizhenie2(Sputnik sp1, Sputnik sp2, Sputnik sp3);//Центр тяжести треугольника
    dannx MZD_sp12 (int kk, double* x, Sputnik sp1, Sputnik sp2, double tau12);
    double function2(double* x, Sputnik sp1, Sputnik sp2, double tau12);
    double function3(double x,GEOD sp1, GEOD sp2,double fi);
    Istochnik k1,k2;
    dann approx_gyper(const dann dann1);
    double function4(double *x,const dann dann1);
    double gyperbol(double x,const dann dann10);
    /// ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Sputnik sputnik1;
    Sputnik sputnik2;
    Sputnik sputnik3;
    Istochnik istochnik1;
    Istochnik istochnik2;
    Data data;

    double tau12, tau13;//временные задержки
    double minY=0,maxY=0,minX=0,maxX=0;
    QVector<double> x,y,x1,y1,x2,y2,x3,y3,x4,y4;
    struct sv
    {
       QVector<double> x;
       QVector<double> y;
    };
    ///На всякий случай
    sv zh;
    sv krug_zhizny(QVector<double> x1,QVector<double> y1);
};

#endif // MAINWIDGET_H
