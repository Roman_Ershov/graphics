#-------------------------------------------------
#
# Project created by QtCreator 2017-10-20T16:24:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Kursovoy_proekt
TEMPLATE = app


SOURCES += main.cpp\
        mainwidget.cpp \
    qcustomplot.cpp

HEADERS  += mainwidget.h \
    qcustomplot.h

FORMS    += mainwidget.ui
