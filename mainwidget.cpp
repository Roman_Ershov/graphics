#include "mainwidget.h"
#include "ui_mainwidget.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    ui->lineEdit->setText("25");
    ui->lineEdit_2->setText("25");
    ui->lineEdit_3->setText("0");
    ui->lineEdit_4->setText("0");
    ui->lineEdit_5->setText("0");
    ui->lineEdit_6->setText("10207.1");
    ui->lineEdit_7->setText("8706.89");
    ui->lineEdit_8->setText("9394.15");
    ui->lineEdit_9->setText("10764");
    ui->lineEdit_10->setText("10979");
    ui->lineEdit_11->setText("8099.3");
    ui->lineEdit_12->setText("4000");
    ui->lineEdit_13->setText("9253.45");
    ui->lineEdit_14->setText("5760.78");
    data.day = 10;
    data.month = 10;
    data.year = 2017;
    QPen greenpen(Qt::green);
    QPen yellowpen(Qt::yellow);
    QPen pinkpen(Qt::magenta);
}

MainWidget::~MainWidget()
{
    delete ui;
}

/** Пишем комментарии к функциям */
double MainWidget::Vichislenie_Zv_vremya_Grinvich_v_Polnoch(Data data)
{
    int a = (14 - data.month) / 12;
        int y = data.year + 4800 - a;
        int m = data.month + 12 * a - 3;

        //// Номер юлианского дня ? (правильно понял?)
        int JDN = data.day + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045;

        /// Нужна ссылка на эту формулу
        /// И почему в полночь? От текущего времени перевод тоже должен зависеть
        double d = (JDN - 2451545) / 36525;
        double Scp = 1.7533685592 + 0.0172027918051*(JDN - 2451545) + 6.7707139*(1e-6)*d*d - 4.50876*(1e-10)*d*d*d;
        //double Scp = (6 * 3600 + 41 * 60 + 50.54641 + 236.555367908*(JDN - 2451545) + 0.093104*d*d - 6.2*(1e-6)*d*d*d)/180*PI;

        return Scp;
}

//// Смысл параметра phi ?
double MainWidget::radius_of_Earth(double phi)//Считаем радиус Земли
{
    double Re = 6378.2;//Экваториальный радиус Земли
        /*double Rp = 6356.86;//Полярный радиус Земли
        double R;//Радиус Земли на произвольной широте
        R = cos(phi)*(Re - Rp) + Rp;//Расчет радиуса Земли
        cout << "Radius Earth " << R << endl;*/
        double R = Re;
        return R;
}

double *MainWidget::Pol_t_P(double xp, double yp)
{
    double mas[9] = { 1,0,xp,0,1,-yp,-xp,yp,1 };
        double *p1 = mas;
        return p1;
}



double *MainWidget::Zv_vremya_P(double vremya)//Матрица звездного времени на Гринвиче в определенную дату
{
    double ugol = vremya;
        double mas[9] = { cos(ugol),sin(ugol),0 ,-sin(ugol),cos(ugol),0, 0,0,1 };
        //// почему не возвращать просто mas ?
        double *p2 = mas;
        return p2;
}

double *MainWidget::Pol_t_O(double xp, double yp) //Транспонированная матрица
{
    double mas[9] = { 1,0,-xp,0,1,yp,xp,-yp,1 };
        double *p3 = mas;
        return p3;
}

double *MainWidget::Zv_vremya_O(double vremya)//Транспонированная матрица
{
    double ugol = vremya;
        double mas[9] = { cos(ugol),-sin(ugol),0 ,sin(ugol),cos(ugol),0, 0,0,1 };
        double *p4 = mas;
        return p4;
}

MainWidget::GRINVICH MainWidget::Perevod_is_IGASK_v_Grinvich(MainWidget::IGASK &koordinaty, MainWidget::Data data)
{
    double *mas = Pol_t_P(0.03, 0.35);    /// Координаты мгновенного полюса тоже вроде как должны зависеть от времени
        double pros = Vichislenie_Zv_vremya_Grinvich_v_Polnoch(data);
        //double pros = (0 * 15 + (43 * 15) / 60 + (36.5884 * 15) / 3600) / 180 * PI;
        //cout << "DATA " << data.year << " " << data.month << " " << data.day << endl;
        double *mas1 = Zv_vremya_P(pros);
        double mas11[9]={0,0,0,0,0,0,0,0,0};

        mas11[0] = mas[0] * mas1[0] + mas[1] * mas1[3] + mas[2] * mas1[6];
        mas11[1] = mas[0] * mas1[1] + mas[1] * mas1[4] + mas[2] * mas1[7];
        mas11[2] = mas[0] * mas1[2] + mas[1] * mas1[5] + mas[2] * mas1[8];

        mas11[3] = mas[3] * mas1[0] + mas[4] * mas1[3] + mas[5] * mas1[6];
        mas11[4] = mas[3] * mas1[1] + mas[4] * mas1[4] + mas[5] * mas1[7];
        mas11[5] = mas[3] * mas1[2] + mas[4] * mas1[5] + mas[5] * mas1[8];

        mas11[6] = mas[6] * mas1[0] + mas[7] * mas1[3] + mas[8] * mas1[6];
        mas11[7] = mas[6] * mas1[1] + mas[7] * mas1[4] + mas[8] * mas1[7];
        mas11[8] = mas[6] * mas1[2] + mas[7] * mas1[5] + mas[8] * mas1[8];


        double mas2[3] = { koordinaty.x,koordinaty.y,koordinaty.z };
        GRINVICH grin;

        grin.x = mas11[0] * mas2[0] + mas11[1] * mas2[1] + mas11[2] * mas2[2];
        grin.y = mas11[3] * mas2[0] + mas11[4] * mas2[1] + mas11[5] * mas2[2];
        grin.z = mas11[6] * mas2[0] + mas11[7] * mas2[1] + mas11[8] * mas2[2];

        return grin;
}

MainWidget::IGASK MainWidget::Perevod_is_Grinvich_v_IGASK(MainWidget::GRINVICH &koordinaty, MainWidget::Data data)
{
    double *mas = Pol_t_O(0.03, 0.35);
        double pros = Vichislenie_Zv_vremya_Grinvich_v_Polnoch(data);
        //double pros = (0 * 15 + (43 * 15) / 60 + (36.5884 * 15) / 3600) / 180 * PI;
        //cout << "DATA " << data.year << " " << data.month << " " << data.day << endl;
        //cout << pros << endl;
        double *mas1 = Zv_vremya_O(pros);
        double mas11[9]={0,0,0,0,0,0,0,0,0};

        mas11[0] = mas[0] * mas1[0] + mas[1] * mas1[3] + mas[2] * mas1[6];
        mas11[1] = mas[0] * mas1[1] + mas[1] * mas1[4] + mas[2] * mas1[7];
        mas11[2] = mas[0] * mas1[2] + mas[1] * mas1[5] + mas[2] * mas1[8];

        mas11[3] = mas[3] * mas1[0] + mas[4] * mas1[3] + mas[5] * mas1[6];
        mas11[4] = mas[3] * mas1[1] + mas[4] * mas1[4] + mas[5] * mas1[7];
        mas11[5] = mas[3] * mas1[2] + mas[4] * mas1[5] + mas[5] * mas1[8];

        mas11[6] = mas[6] * mas1[0] + mas[7] * mas1[3] + mas[8] * mas1[6];
        mas11[7] = mas[6] * mas1[1] + mas[7] * mas1[4] + mas[8] * mas1[7];
        mas11[8] = mas[6] * mas1[2] + mas[7] * mas1[5] + mas[8] * mas1[8];


        double mas2[3] = { koordinaty.x,koordinaty.y,koordinaty.z };
        IGASK igask;

        igask.x = mas11[0] * mas2[0] + mas11[1] * mas2[1] + mas11[2] * mas2[2];
        igask.y = mas11[3] * mas2[0] + mas11[4] * mas2[1] + mas11[5] * mas2[2];
        igask.z = mas11[6] * mas2[0] + mas11[7] * mas2[1] + mas11[8] * mas2[2];

        return igask;
}



MainWidget::GEOD MainWidget::perevod_s_IGASK(MainWidget::IGASK &koordinaty, MainWidget::Data data)
{
    GRINVICH grinvich = Perevod_is_IGASK_v_Grinvich(koordinaty, data);
        GEOD prom;//Промежуточные координаты
        prom.shirota = atan(grinvich.z / sqrt(grinvich.x*grinvich.x + grinvich.y*grinvich.y))* 180.0 / PI;//Широта
        prom.dolgota = atan(grinvich.y / grinvich.x)* 180.0 / PI;//Долгота
        double shirota = prom.shirota / 180 * PI;
        prom.visota = sqrt(grinvich.x*grinvich.x + grinvich.y*grinvich.y + grinvich.z*grinvich.z) - radius_of_Earth(shirota);//Высота
        return prom;
}

MainWidget::IGASK MainWidget::perevod_s_GEOD(MainWidget::GEOD &koordinaty, MainWidget::Data data)
{
    koordinaty.shirota = koordinaty.shirota / 180 * PI;
        koordinaty.dolgota = koordinaty.dolgota / 180 * PI;
        double phi = koordinaty.shirota;
        double H = koordinaty.visota;
        double R = radius_of_Earth(phi);
        GRINVICH prom;//Промежуточные координаты
        prom.x = (R + H)*cos(koordinaty.shirota)*cos(koordinaty.dolgota);//Координата X
        prom.y = (R + H)*cos(koordinaty.shirota)*sin(koordinaty.dolgota);//Координата Y
        prom.z = (R + H)*sin(koordinaty.shirota);//Координата Z
        IGASK prom1 = Perevod_is_Grinvich_v_IGASK(prom, data);
        return prom1;
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////Разностно-дальномерный метод//////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/// Надо писать комментарии, а то с первого раза не понять, что делает эта функция
QVector<double> MainWidget::reshenie_sistemy(double Cx, double Cy, double Bx, double By, double Mx, double My, double Px, double Py)
{
    double x = ((Py - Cy) / (Px - Cx)*Cx - (My - By) / (Mx - Bx)*Bx + By - Cy) / ((Py - Cy) / (Px - Cx) - (My - By) / (Mx - Bx));
    double y = (Py - Cy)*(x - Cx) / (Px - Cx)+Cy;
    QVector<double> k = { x,y };
    return k;
}

QVector<double> MainWidget::nach_priblizhenie(Sputnik sp1, Sputnik sp2, Sputnik sp3)//Центр тяжести треугольника
{
    double seredina12_shirota = (sp1.koord_sputnik_GEOD.shirota + sp2.koord_sputnik_GEOD.shirota) / 2;
    double seredina12_dolgota = (sp1.koord_sputnik_GEOD.dolgota + sp2.koord_sputnik_GEOD.dolgota) / 2;
    double seredina13_shirota = (sp1.koord_sputnik_GEOD.shirota + sp3.koord_sputnik_GEOD.shirota) / 2;
    double seredina13_dolgota = (sp1.koord_sputnik_GEOD.dolgota + sp3.koord_sputnik_GEOD.dolgota) / 2;
    double Cx = sp3.koord_sputnik_GEOD.dolgota, Cy = sp3.koord_sputnik_GEOD.shirota
        , Bx = sp2.koord_sputnik_GEOD.dolgota, By = sp2.koord_sputnik_GEOD.shirota
        , Mx = seredina13_dolgota, My = seredina13_shirota
        , Px = seredina12_dolgota, Py = seredina12_shirota;
    QVector<double> k = reshenie_sistemy(Cx, Cy, Bx, By, Mx, My, Px, Py);
    return k;
}

double MainWidget::function(double* x, Sputnik sp1, Sputnik sp2, Sputnik sp3, double tau12, double tau13)
{
    // Реализует оптимизируемую функцию
    // Возвращает значение функции
    // количество параметров является членом класса, в противном случае изменить сигнатуру функции
    //	........
    double R = 6378.2;
    //float c = 299792.458;
    /*float x1 = 10000, y1 = 10000, z1 = 9600;
    float x2 = 10000, y2 = 10000, z2 = 10000;
    float x3 = 10000, y3 = 10000, z3 = 10400;*/
    double x1 = sp1.koord_sputnik_IGASK.x, y1 = sp1.koord_sputnik_IGASK.y, z1 = sp1.koord_sputnik_IGASK.z;
    double x2 = sp2.koord_sputnik_IGASK.x, y2 = sp2.koord_sputnik_IGASK.y, z2 = sp2.koord_sputnik_IGASK.z;
    double x3 = sp3.koord_sputnik_IGASK.x, y3 = sp3.koord_sputnik_IGASK.y, z3 = sp3.koord_sputnik_IGASK.z;

    //double tau12 = 0.00001, tau13 = 0.00003;

    ///// Во-первых, нужно здесь учесть сплюснутость Земли у полюсов
    /// Нужно ввести коэффициент 1.0 / 298.26; и делать поправки на него при пересчете координат на поверхности
    /// Во-вторых, т.к. при вычислениях используется R^2, то можно сразу объявить переменную R^2
    /// В-третьих, предлагаю оптимизацию, которая может ускорить процесс сходимости:
    /// предварительно выразить, например, координату z уз уравнения Земли (с учетом сплюснутости)
    /// и принудительно задать координату z получившемуся результату. Так мы учтем уравнение Земли
    /// А функционал уже рассчитывать только с учетом задержек

    //x[0] - координата х источника
    //x[1] - координата y источника
    //x[2] - координата z источника
    double k =
        //////////////////////////////////////x^2+y^2+z^2 = R^2//////////////////////////////////////
        (x[0] * x[0] + x[1] * x[1] + x[2] * x[2] - R*R) * (x[0] * x[0] + x[1] * x[1] + x[2] * x[2] - R*R)

            ///// Т.к. задержки могут быть отрицательными (см. ниже), модуль надо убрать
        //////////////////////////////////L1-L2 = c*tau12////////////////////////////////////////////
        + (abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
            - sqrt((x[0] - x2)*(x[0] - x2) + (x[1] - y2)*(x[1] - y2) + (x[2] - z2)*(x[2] - z2))) - c*tau12)
        *(abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
            - sqrt((x[0] - x2)*(x[0] - x2) + (x[1] - y2)*(x[1] - y2) + (x[2] - z2)*(x[2] - z2))) - c*tau12)

        //////////////////////////////////L1-L3 = c*tau13////////////////////////////////////////////
        + (abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
            - sqrt((x[0] - x3)*(x[0] - x3) + (x[1] - y3)*(x[1] - y3) + (x[2] - z3)*(x[2] - z3))) - c*tau13)
        *(abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
            - sqrt((x[0] - x3)*(x[0] - x3) + (x[1] - y3)*(x[1] - y3) + (x[2] - z3)*(x[2] - z3))) - c*tau13);
    return k;
}

void MainWidget::MHJ(int kk, double* x, Sputnik sp1, Sputnik sp2, Sputnik sp3, Istochnik &istochnik, double tau12, double tau13,Data data)
{
    // kk - количество параметров; x - массив параметров
    double  TAU = 1.e-16f; // Точность вычислений
    int i, j, bs, ps;
    double z, h, k, fi, fb;
    double *b = new double[kk];
    double *y = new double[kk];
    double *p = new double[kk];
    QVector<double> koefg = nach_priblizhenie(sp1, sp2, sp3);
    GEOD prom;
    prom.dolgota = koefg[0];
    prom.shirota = koefg[1];
    prom.visota = 0;
    x2.push_back(prom.dolgota);
    y2.push_back(prom.shirota);
    IGASK prom1 = perevod_s_GEOD(prom, data);
    h = 1.;
    x[0] = prom1.x;
    x[1] = prom1.y;
    x[2] = prom1.z;
    /*cout << prom.dolgota << " " << prom.shirota << " " << prom.visota << endl;
    cout << x[0] << " " << x[1] << " " << x[2] << endl;*/
    //for (i = 0; i<kk; i++)  x[i] = (float)rand() / RAND_MAX; // Задается начальное приближение

    k = h;
    for (i = 0; i < kk; i++)	y[i] = p[i] = b[i] = x[i];
    fi = function(x, sp1, sp2, sp3, tau12, tau13);
    ps = 0;   bs = 1;  fb = fi;

    j = 0;
    while (1)
    {
        //calc++; // Счетчик итераций. Можно игнорировать

        x[j] = y[j] + k;
        z = function(x, sp1, sp2, sp3, tau12, tau13);
        if (z >= fi) {
            x[j] = y[j] - k;
            z = function(x, sp1, sp2, sp3, tau12, tau13);
            if (z < fi)   y[j] = x[j];
            else  x[j] = y[j];
        }
        else  y[j] = x[j];
        fi = function(x, sp1, sp2, sp3, tau12, tau13);

        if (j < kk - 1) { j++;  continue; }
        if (fi + 1e-16 >= fb) {
            if (ps == 1 && bs == 0) {
                for (i = 0; i < kk; i++) {
                    p[i] = y[i] = x[i] = b[i];
                }
                z = function(x, sp1, sp2, sp3, tau12, tau13);
                bs = 1;   ps = 0;   fi = z;   fb = z;   j = 0;
                continue;
            }
            k /= 10;
            if (k < TAU) break;
            //double zzz = function(x, sp1, sp2, sp3, tau12, tau13);
            //if (z < 1) break;
            j = 0;
            continue;
        }

        for (i = 0; i < kk; i++) {
            p[i] = 2 * y[i] - b[i];
            b[i] = y[i];
            x[i] = p[i];

            y[i] = x[i];
        }
        z = function(x, sp1, sp2, sp3, tau12, tau13);
        fb = fi;   ps = 1;   bs = 0;   fi = z;   j = 0;
    } //  end of while(1)

    for (i = 0; i < kk; i++)  x[i] = p[i];
    //cout << x[0] << " " << x[1] << " " << x[2] << " " << fb << endl;
    istochnik.koord_istochnik_IGASK.x = x[0];
    istochnik.koord_istochnik_IGASK.y = x[1];
    istochnik.koord_istochnik_IGASK.z = x[2];
    delete b;
    delete y;
    delete p;
}

double MainWidget::vremennye_zaderzhki(Istochnik istochnik, Sputnik sputnik1, Sputnik sputnik2)
{
    double L1 = sqrt((sputnik1.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
        *(sputnik1.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
        + (sputnik1.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
        *(sputnik1.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
        + (sputnik1.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z)
        *(sputnik1.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z));
    double L2 = sqrt((sputnik2.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
        *(sputnik2.koord_sputnik_IGASK.x - istochnik.koord_istochnik_IGASK.x)
        + (sputnik2.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
        *(sputnik2.koord_sputnik_IGASK.y - istochnik.koord_istochnik_IGASK.y)
        + (sputnik2.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z)
        *(sputnik2.koord_sputnik_IGASK.z - istochnik.koord_istochnik_IGASK.z));
    double tau = abs(L1 / c - L2 / c);  ///// Вот здесь не обязательно брать модуль!!!
    /// Задержки могут быть и отрицательными в зависимости от того какой спутник будет опорным.
    return tau;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////Линии положения///////////////////////////////////////////////////////

//// Не понимаю, что делает эта функция :(
double MainWidget::function3(double x,GEOD sp1,GEOD sp2,double fi)
{
    double y;
    y = x*tan(fi) - sp1.dolgota*(sp2.shirota-sp1.shirota)/(sp2.dolgota-sp1.dolgota)+sp1.shirota;
    return y;
}
///////////////////////////////////////////////Аппроксимация//////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
double MainWidget::function4(double *x, const MainWidget::dann dann1)
{
    double k = 0;
    for(int i=0;i<dann1.x.size();i++)
    {
      k+=((abs(dann1.x[i]*dann1.x[i]/x[0]/x[0]- dann1.y[i]*dann1.y[i]/x[1]/x[1])- 1)*(abs(dann1.x[i]*dann1.x[i]/x[0]/x[0]- dann1.y[i]*dann1.y[i]/x[1]/x[1])- 1));
    }
    return k;
}


MainWidget::dann MainWidget::approx_gyper(const MainWidget::dann dann1)
{
    dann prom1;
    // kk - количество параметров; x - массив параметров
      float  TAU=1.e-6f; // Точность вычислений
      int i, j, bs, ps;
      float z, h, k, fi, fb;
      int kk = 2;
      float *b = new float[kk];
      float *y = new float[kk];
      float *p = new float[kk];
      QFile fileOut("fileout2.txt"); // Связываем объект с файлом fileout.txt
      if(fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
          { // Если файл успешно открыт для записи в текстовом режиме
              //QTextStream writeStream(&fileOut); // Создаем объект класса QTextStream
      // и передаем ему адрес объекта fileOut
              // Посылаем строку в поток для записи
              //fileOut.close(); // Закрываем файл
          }
      QTextStream writeStream(&fileOut);
      double x[2];
      h=10;
      x[0]=30;
      x[1]=30;
      //for( i=1; i<kk; i++)  x[i]=(float)rand()/RAND_MAX; // Задается начальное приближение

      k = h;
      for( i=0; i<kk; i++)	y[i] = p[i] = b[i] = x[i];
      fi = function4(x,dann1);
      ps = 0;   bs = 1;  fb = fi;
      writeStream<<dann1.x[0]<<" "<<dann1.y[0]<<" "<<fi<<endl;
      j = 0;
      while(1)
      {


      x[j] = y[j] + k;
      z = function4(x,dann1);
      if (z >= fi) {
         x[j] = y[j] - k;
         z = function4(x,dann1);
         if( z<fi )   y[j] = x[j];
         else  x[j] = y[j];
         }
      else  y[j] = x[j];
      fi = function4(x,dann1);

      if ( j < kk-1 )   {  j++;  continue;  }
      if ( fi + 1e-8 >= fb )  {
         if ( ps == 1 && bs == 0) {
            for( i=0; i<kk; i++)	 {
              p[i] = y[i] = x[i] = b[i];
              }
            z = function4(x,dann1);
            bs = 1;   ps = 0;   fi = z;   fb = z;   j = 0;
            continue;
            }
     k /= 10.;
     if ( k < TAU ) break;
     //if(z<10)break;
      j = 0;
      continue;
      }

        for( i=0; i<kk; i++) {
          p[i] = 2 * y[i] - b[i];
          b[i] = y[i];
          x[i] = p[i];

          y[i] = x[i];
          }
        z = function4(x,dann1);
        writeStream<<z<<endl;
        fb = fi;   ps = 1;   bs = 0;   fi = z;   j = 0;
      } //  end of while(1)

      for( i=0; i<kk; i++)  x[i] = p[i];

      delete b;
      delete y;
      delete p;

      prom1.x.push_back(x[0]);
      prom1.y.push_back(x[1]);
      return prom1;
}


double MainWidget::gyperbol(double x,const MainWidget::dann dann10)
{
    double y=sqrt((x*x/dann10.x[0]/dann10.x[0]-1)*dann10.y[0]*dann10.y[0]);
    return y;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////  Чем отличаются приведенные ниже две функции от аналогичных функций выше?
QVector<double> MainWidget::reshenie_sistemy2(double Cx, double Cy, double Bx, double By, double Mx, double My, double Px, double Py)
{
    double x = ((Py - Cy) / (Px - Cx)*Cx - (My - By) / (Mx - Bx)*Bx + By - Cy) / ((Py - Cy) / (Px - Cx) - (My - By) / (Mx - Bx));
    double y = (Py - Cy)*(x - Cx) / (Px - Cx)+Cy;
    QVector<double> k = { x,y };
    return k;
}

QVector<double> MainWidget::nach_priblizhenie2(Sputnik sp1, Sputnik sp2, Sputnik sp3)//Центр тяжести треугольника
{
    double seredina12_shirota = (sp1.koord_sputnik_GEOD.shirota + sp2.koord_sputnik_GEOD.shirota) / 2;
    double seredina12_dolgota = (sp1.koord_sputnik_GEOD.dolgota + sp2.koord_sputnik_GEOD.dolgota) / 2;
    double seredina13_shirota = (sp1.koord_sputnik_GEOD.shirota + sp3.koord_sputnik_GEOD.shirota) / 2;
    double seredina13_dolgota = (sp1.koord_sputnik_GEOD.dolgota + sp3.koord_sputnik_GEOD.dolgota) / 2;
    double Cx = sp3.koord_sputnik_GEOD.dolgota, Cy = sp3.koord_sputnik_GEOD.shirota
        , Bx = sp2.koord_sputnik_GEOD.dolgota, By = sp2.koord_sputnik_GEOD.shirota
        , Mx = seredina13_dolgota, My = seredina13_shirota
        , Px = seredina12_dolgota, Py = seredina12_shirota;
    QVector<double> k = reshenie_sistemy(Cx, Cy, Bx, By, Mx, My, Px, Py);
    return k;
}

/////// вычисление функционала для для построения линий положения
double MainWidget::function2(double *x, MainWidget::Sputnik sp1, MainWidget::Sputnik sp2, double tau12)
{
    double R = 6378.2;
    //float c = 299792.458;
    /*float x1 = 10000, y1 = 10000, z1 = 9600;
    float x2 = 10000, y2 = 10000, z2 = 10000;
    float x3 = 10000, y3 = 10000, z3 = 10400;*/
    double x1 = sp1.koord_sputnik_IGASK.x, y1 = sp1.koord_sputnik_IGASK.y, z1 = sp1.koord_sputnik_IGASK.z;
    double x2 = sp2.koord_sputnik_IGASK.x, y2 = sp2.koord_sputnik_IGASK.y, z2 = sp2.koord_sputnik_IGASK.z;
    GEOD prom1;
    IGASK prom2;
    prom1.dolgota = x[0];
    prom1.shirota = x[1];
    prom1.visota = 0;
    prom2 = perevod_s_GEOD(prom1,data);
    x[0] = prom2.x;
    x[1] = prom2.y;
    x[2] = prom2.z;
    //double tau12 = 0.00001, tau13 = 0.00003;
    //x[0] - координата х источника
    //x[1] - координата y источника
    //x[2] - координата z источника

    ////// Здесь выше уже фактически учтено уравнение Земли (только, по-прежнему нужно учесть
    /// сплюснутость) - высота нахождения источника задана равной нулю, так что
    /// вклад Земли в функционал ниже лишний

    double k =
        //////////////////////////////////////x^2+y^2+z^2 = R^2//////////////////////////////////////
        (x[0] * x[0] + x[1] * x[1] + x[2] * x[2] - R*R) * (x[0] * x[0] + x[1] * x[1] + x[2] * x[2] - R*R)

        //////////////////////////////////L1-L2 = c*tau12////////////////////////////////////////////
        + (abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
            - sqrt((x[0] - x2)*(x[0] - x2) + (x[1] - y2)*(x[1] - y2) + (x[2] - z2)*(x[2] - z2))) - c*tau12)
        *(abs(sqrt((x[0] - x1)*(x[0] - x1) + (x[1] - y1)*(x[1] - y1) + (x[2] - z1)*(x[2] - z1))
            - sqrt((x[0] - x2)*(x[0] - x2) + (x[1] - y2)*(x[1] - y2) + (x[2] - z2)*(x[2] - z2))) - c*tau12);
    x[0]=prom1.dolgota*180/PI;
    x[1]=prom1.shirota*180/PI;
    return k;
}

////////// Метод Хука-Дживса для расчета линии положения
MainWidget::dannx MainWidget::MZD_sp12(int kk, double *x, MainWidget::Sputnik sp1, MainWidget::Sputnik sp2, double tau12)
{
    // kk - количество параметров; x - массив параметров
    double  TAU = 1.e-8; // Точность вычислений
    int i, j, bs, ps;
    double z, h, k, fi, fb;
    double *b = new double[kk];
    double *y = new double[kk];
    double *p = new double[kk];
    dannx dann1;
    QFile fileOut("fileout.txt"); // Связываем объект с файлом fileout.txt
        if(fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
        { // Если файл успешно открыт для записи в текстовом режиме
            //QTextStream writeStream(&fileOut); // Создаем объект класса QTextStream
    // и передаем ему адрес объекта fileOut
            //writeStream << fi<<endl; // Посылаем строку в поток для записи
            //fileOut.close(); // Закрываем файл
        }
    QTextStream writeStream(&fileOut); // Создаем объект класса QTextStream
    double ugol=0;


    while(ugol<360)
    {

    h = 13; ////// ???????
    x[0] = sp1.koord_sputnik_GEOD.dolgota;
    x[1] = sp1.koord_sputnik_GEOD.shirota;
    x[2] = 0;
    k = h;
    for (i = 0; i < kk; i++)	y[i] = p[i] = b[i] = x[i];

    fi = function2(x, sp1, sp2, tau12);

    ps = 0;   bs = 1;  fb = fi;

    j = 0;
    while (1)
    {
        //calc++; // Счетчик итераций. Можно игнорировать

        x[j] = y[j] - k;
        double pin1 = ugol*180/PI;
        x[j+1]= function3(x[j],sp1.koord_sputnik_GEOD,sp2.koord_sputnik_GEOD,pin1);
        z = function2(x, sp1, sp2, tau12);
        //writeStream<<z<<" "<<x[0]<<" "<<x[1]<<" "<<x[2]<<endl;
        if (z >= fi) {
            x[j] = y[j] + k;
            x[j+1]= function3(x[j],sp1.koord_sputnik_GEOD,sp2.koord_sputnik_GEOD,pin1);
            z = function2(x, sp1, sp2, tau12);
            if (z < fi)
            {
                y[j] = x[j];
                y[j+1] = x[j+1];
            }
            else
            {
                x[j] = y[j];
                x[j+1] = y[j+1];
            }
        }
        else
        {
            y[j] = x[j];
            y[j+1] = x[j+1];
        }
        fi = function2(x, sp1, sp2, tau12);

        if (fi + 1e-8 >= fb) {
            if (ps == 1 && bs == 0) {
                for (i = 0; i < kk; i++) {
                    p[i] = y[i] = x[i] = b[i];
                }
                z = function2(x, sp1, sp2, tau12);
                bs = 1;   ps = 0;   fi = z;   fb = z;   j = 0;
                continue;
            }
            k /= 10;
            if (k < TAU) break;
            //double zzz = function(x, sp1, sp2, sp3, tau12, tau13);
            //if (z < 1e-3) break;
            j = 0;
            continue;
        }
        for (i = 0; i < kk; i++) {
            p[i] = 2 * y[i] - b[i];
            b[i] = y[i];
            x[i] = p[i];

            y[i] = x[i];
        }
        z = function2(x, sp1, sp2, tau12);
        //writeStream<<z<<endl;
        fb = fi;   ps = 1;   bs = 0;   fi = z;   j = 0;
    } //  end of while(1)
    writeStream<<z<<endl;
    for (i = 0; i < kk; i++)  x[i] = p[i];
    if(z<1)
    {
    dann1.x.push_back(x[0]);
    dann1.y.push_back(x[1]);
    }
    ugol+=1.3;
   }

    /////// А второй проход зачем?
        //if(fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
        //{ // Если файл успешно открыт для записи в текстовом режиме

    // и передаем ему адрес объекта fileOut
            //writeStream << x[0]<<endl<<x[1]<<endl<<x[2]<<endl<<fb<<endl; // Посылаем строку в поток для записи
            fileOut.close(); // Закрываем файл
        //}     
            ugol=0;
            while(ugol<360)
            {

            h = 13;
            x[0] = sp2.koord_sputnik_GEOD.dolgota;
            x[1] = sp2.koord_sputnik_GEOD.shirota;
            x[2] = 0;
            k = h;
            for (i = 0; i < kk; i++)	y[i] = p[i] = b[i] = x[i];

            fi = function2(x, sp1, sp2, tau12);

            ps = 0;   bs = 1;  fb = fi;

            j = 0;
            while (1)
            {
                //calc++; // Счетчик итераций. Можно игнорировать

                x[j] = y[j] + k;
                double pin1 = ugol*180/PI;
                x[j+1]= function3(x[j],sp1.koord_sputnik_GEOD,sp2.koord_sputnik_GEOD,pin1);
                z = function2(x, sp1, sp2, tau12);
                //writeStream<<z<<" "<<x[0]<<" "<<x[1]<<" "<<x[2]<<endl;
                if (z >= fi) {
                    x[j] = y[j] - k;
                    x[j+1]= function3(x[j],sp1.koord_sputnik_GEOD,sp2.koord_sputnik_GEOD,pin1);
                    z = function2(x, sp1, sp2, tau12);
                    if (z < fi)
                    {
                        y[j] = x[j];
                        y[j+1] = x[j+1];
                    }
                    else
                    {
                        x[j] = y[j];
                        x[j+1] = y[j+1];
                    }
                }
                else
                {
                    y[j] = x[j];
                    y[j+1] = x[j+1];
                }
                fi = function2(x, sp1, sp2, tau12);

                if (fi + 1e-8 >= fb) {
                    if (ps == 1 && bs == 0) {
                        for (i = 0; i < kk; i++) {
                            p[i] = y[i] = x[i] = b[i];
                        }
                        z = function2(x, sp1, sp2, tau12);
                        bs = 1;   ps = 0;   fi = z;   fb = z;   j = 0;
                        continue;
                    }
                    k /= 10;
                    if (k < TAU) break;
                    //double zzz = function(x, sp1, sp2, sp3, tau12, tau13);
                    //if (z < 1e-3) break;
                    j = 0;
                    continue;
                }
                for (i = 0; i < kk; i++) {
                    p[i] = 2 * y[i] - b[i];
                    b[i] = y[i];
                    x[i] = p[i];

                    y[i] = x[i];
                }
                z = function2(x, sp1, sp2, tau12);
                //writeStream<<z<<endl;
                fb = fi;   ps = 1;   bs = 0;   fi = z;   j = 0;
            } //  end of while(1)
            writeStream<<z<<endl;
            for (i = 0; i < kk; i++)  x[i] = p[i];
            if(z<1)
            {
            dann1.x1.push_back(x[0]);
            dann1.y1.push_back(x[1]);
            }
            ugol+=1.3;
           }
                //if(fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
                //{ // Если файл успешно открыт для записи в текстовом режиме

            // и передаем ему адрес объекта fileOut
                    //writeStream << x[0]<<endl<<x[1]<<endl<<x[2]<<endl<<fb<<endl; // Посылаем строку в поток для записи
                    fileOut.close(); // Закрываем файл
                //}
            delete b;
            delete y;
            delete p;
    return dann1;
}

















/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*MainWidget::sv MainWidget::krug_zhizny(QVector<double> x1, QVector<double> y1)
{
    sv sh;
    double k1 = x1[0],k2 = y1[0],m1 =40.,m2 = 25.,p0=0;
    for(double i=0;i<360;i++)
    {
        p0 =i/180*PI;
        sh.x.push_back(k1+m1*cos(p0));
        sh.y.push_back(k2+m2*sin(p0));
    }
    return sh;
}*/


void MainWidget::on_Rasschet_clicked()
{
    S1 = ui->lineEdit->text();
    istochnik1.koord_istochnik_GEOD.shirota = S1.toDouble();
    S2 = ui->lineEdit_2->text();
    istochnik1.koord_istochnik_GEOD.dolgota = S2.toDouble();
    S3 = ui->lineEdit_3->text();
    istochnik1.koord_istochnik_GEOD.visota = S3.toDouble();
    S4 = ui->lineEdit_4->text();
    tau12 = S4.toDouble();
    S5 = ui->lineEdit_5->text();
    tau13 = S5.toDouble();

    S6 = ui->lineEdit_6->text();
    sputnik1.koord_sputnik_IGASK.x = S6.toDouble();
    S7 = ui->lineEdit_7->text();
    sputnik1.koord_sputnik_IGASK.y = S7.toDouble();
    S8 = ui->lineEdit_8->text();
    sputnik1.koord_sputnik_IGASK.z = S8.toDouble();

    S9 = ui->lineEdit_9->text();
    sputnik2.koord_sputnik_IGASK.x = S9.toDouble();
    S10 = ui->lineEdit_10->text();
    sputnik2.koord_sputnik_IGASK.y = S10.toDouble();
    S11 = ui->lineEdit_11->text();
    sputnik2.koord_sputnik_IGASK.z = S11.toDouble();

    S12 = ui->lineEdit_12->text();
    sputnik3.koord_sputnik_IGASK.x = S12.toDouble();
    S13 = ui->lineEdit_13->text();
    sputnik3.koord_sputnik_IGASK.y = S13.toDouble();
    S14 = ui->lineEdit_14->text();
    sputnik3.koord_sputnik_IGASK.z = S14.toDouble();


    //Рассчеты
    sputnik1.koord_sputnik_GEOD = perevod_s_IGASK(sputnik1.koord_sputnik_IGASK, data);
    sputnik2.koord_sputnik_GEOD = perevod_s_IGASK(sputnik2.koord_sputnik_IGASK, data);
    sputnik3.koord_sputnik_GEOD = perevod_s_IGASK(sputnik3.koord_sputnik_IGASK, data);
    QFile fileOut("fileout1.txt"); // Связываем объект с файлом fileout.txt
        if(fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
        { // Если файл успешно открыт для записи в текстовом режиме
            QTextStream writeStream(&fileOut); // Создаем объект класса QTextStream
    // и передаем ему адрес объекта fileOut
            writeStream << sputnik1.koord_sputnik_GEOD.dolgota<<" "
                        << sputnik1.koord_sputnik_GEOD.shirota<<" "
                        << sputnik1.koord_sputnik_GEOD.visota<<" "<<endl

                        << sputnik2.koord_sputnik_GEOD.dolgota<<" "
                                                << sputnik2.koord_sputnik_GEOD.shirota<<" "
                                                << sputnik2.koord_sputnik_GEOD.visota<<" "<<endl

                                                << sputnik3.koord_sputnik_GEOD.dolgota<<" "
                                                                        << sputnik3.koord_sputnik_GEOD.shirota<<" "
                                                                        << sputnik3.koord_sputnik_GEOD.visota<<" "<<endl; // Посылаем строку в поток для записи
            //fileOut.close(); // Закрываем файл
        }

    //Временные задержки
    istochnik1.koord_istochnik_IGASK = perevod_s_GEOD(istochnik1.koord_istochnik_GEOD, data);
    if(tau12==0&&tau13==0)
    {
    tau12 = vremennye_zaderzhki(istochnik1, sputnik1, sputnik2);
    tau13 = vremennye_zaderzhki(istochnik1, sputnik1, sputnik3);
    }
    //Координаты источника
    double *x = new double[3];
    MHJ(3, x, sputnik1, sputnik2, sputnik3, istochnik2, tau12, tau13,data);

    double *z = new double[3];
    dannx1=MZD_sp12(3,z,sputnik1,sputnik2,tau12);
    //dann5=approx_gyper(dann3);


    double *zz = new double[3];
    dannx2=MZD_sp12(3,zz,sputnik1,sputnik3,tau13);
    //dann6=approx_gyper(dann4);


    istochnik2.koord_istochnik_GEOD = perevod_s_IGASK(istochnik2.koord_istochnik_IGASK, data);
}

void MainWidget::on_dateEdit_userDateChanged(const QDate &date)
{
    this->date = date;
    //ui->lineEdit->setText(this->date.toString());
    data.year = this->date.year();
    data.month = this->date.month();
    data.day = this->date.day();
}

void MainWidget::on_Ish_ist_izl_clicked()
{
    QPen redpen(Qt::red);
    double a = -1; //Начало интервала, где рисуем график по оси Ox
    double b =  1; //Конец интервала, где рисуем график по оси Ox
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(0)->setPen(redpen);
    //ui->graphicsView->graph(0)->setBrush(redbrush);
     ui->graphicsView->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssPlus,30));
     //Подписываем оси Ox и Oy
     ui->graphicsView->xAxis->setLabel("Долгота");
     ui->graphicsView->yAxis->setLabel("Широта");
     ui->graphicsView->xAxis->setRange(a, b);//Для оси Ox
     double promk = istochnik1.koord_istochnik_GEOD.dolgota*180/PI;
     double promk1 = istochnik1.koord_istochnik_GEOD.shirota*180/PI;
     if(minX> promk) minX=1.5* promk;
     if(maxX< promk) maxX=1.5* promk;
     if(minY> promk1) minY=1.5* promk1;
     if(maxY< promk1) maxY=1.5* promk1;
    ui->graphicsView->yAxis->setRange(minY, maxY);//Для оси Oy
    ui->graphicsView->xAxis->setRange(minX, maxX);//Для оси Oy
    x.push_back(istochnik1.koord_istochnik_GEOD.dolgota);
    y.push_back(istochnik1.koord_istochnik_GEOD.shirota);
    x[0]*=180/PI;
    y[0]*=180/PI;
    ui->graphicsView->graph(0)->setData(x, y);
    ui->graphicsView->replot();
}

void MainWidget::on_Sputniky_clicked()
{
    QPen bluepen(Qt::blue);
    //double a = -1; //Начало интервала, где рисуем график по оси Ox
    //double b =  1; //Конец интервала, где рисуем график по оси Ox
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(1)->setPen(bluepen);
    //ui->graphicsView->graph(0)->setBrush(redbrush);
     ui->graphicsView->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssPlus,30));
     //Подписываем оси Ox и Oy
     //ui->graphicsView->xAxis->setLabel("Долгота");
     //ui->graphicsView->yAxis->setLabel("Широта");
     //ui->graphicsView->xAxis->setRange(a, b);//Для оси Ox
     if(minX>sputnik1.koord_sputnik_GEOD.dolgota) minX=1.5*sputnik1.koord_sputnik_GEOD.dolgota;
     if(maxX<sputnik1.koord_sputnik_GEOD.dolgota) maxX=1.5*sputnik1.koord_sputnik_GEOD.dolgota;
     if(minY>sputnik1.koord_sputnik_GEOD.shirota) minY=1.5*sputnik1.koord_sputnik_GEOD.shirota;
     if(maxY<sputnik1.koord_sputnik_GEOD.shirota) maxY=1.5*sputnik1.koord_sputnik_GEOD.shirota;

     if(minX>sputnik2.koord_sputnik_GEOD.dolgota) minX=1.5*sputnik2.koord_sputnik_GEOD.dolgota;
     if(maxX<sputnik2.koord_sputnik_GEOD.dolgota) maxX=1.5*sputnik2.koord_sputnik_GEOD.dolgota;
     if(minY>sputnik2.koord_sputnik_GEOD.shirota) minY=1.5*sputnik2.koord_sputnik_GEOD.shirota;
     if(maxY<sputnik2.koord_sputnik_GEOD.shirota) maxY=1.5*sputnik2.koord_sputnik_GEOD.shirota;

     if(minX>sputnik3.koord_sputnik_GEOD.dolgota) minX=1.5*sputnik3.koord_sputnik_GEOD.dolgota;
     if(maxX<sputnik3.koord_sputnik_GEOD.dolgota) maxX=1.5*sputnik3.koord_sputnik_GEOD.dolgota;
     if(minY>sputnik3.koord_sputnik_GEOD.shirota) minY=1.5*sputnik3.koord_sputnik_GEOD.shirota;
     if(maxY<sputnik3.koord_sputnik_GEOD.shirota) maxY=1.5*sputnik3.koord_sputnik_GEOD.shirota;

    ui->graphicsView->yAxis->setRange(minY, maxY);//Для оси Oy
    ui->graphicsView->xAxis->setRange(minX, maxX);//Для оси Oy
    x1.push_back(sputnik1.koord_sputnik_GEOD.dolgota);
    y1.push_back(sputnik1.koord_sputnik_GEOD.shirota);

    x1.push_back(sputnik2.koord_sputnik_GEOD.dolgota);
    y1.push_back(sputnik2.koord_sputnik_GEOD.shirota);

    x1.push_back(sputnik3.koord_sputnik_GEOD.dolgota);
    y1.push_back(sputnik3.koord_sputnik_GEOD.shirota);
    ui->graphicsView->graph(1)->setData(x1, y1);
    ui->graphicsView->replot();
}

void MainWidget::on_Nach_pribl_clicked()
{
    QPen greenpen(Qt::green);
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(2)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(2)->setPen(greenpen);
    ui->graphicsView->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssPlus,30));
    ui->graphicsView->graph(2)->setData(x2, y2);
    ui->graphicsView->yAxis->setRange(minY, maxY);//Для оси Oy
    ui->graphicsView->xAxis->setRange(minX, maxX);//Для оси Oy
    /*QPen graypen(Qt::gray);
    ui->graphicsView->yAxis->setRange(minY, maxY);//Для оси Oy
    ui->graphicsView->xAxis->setRange(minX, maxX);//Для оси Oy
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(3)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(3)->setPen(graypen);
    ui->graphicsView->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    ui->graphicsView->graph(3)->setData(dann3.x,dann3.y);

    ui->graphicsView->addGraph();
    ui->graphicsView->graph(4)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(4)->setPen(graypen);
    ui->graphicsView->graph(4)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    ui->graphicsView->graph(4)->setData(dann4.x,dann4.y);
    ui->graphicsView->replot();*/
    ui->graphicsView->replot();
}



void MainWidget::on_Pasch_ist_izl_clicked()
{
    QPen magnpen(Qt::black);
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(7)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(7)->setPen(magnpen);
    ui->graphicsView->graph(7)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssPlus,60));
    if(minX>istochnik2.koord_istochnik_GEOD.dolgota) minX=1.5*istochnik2.koord_istochnik_GEOD.dolgota;
    if(maxX<istochnik2.koord_istochnik_GEOD.dolgota) maxX=1.5*istochnik2.koord_istochnik_GEOD.dolgota;
    if(minY>istochnik2.koord_istochnik_GEOD.shirota) minY=1.5*istochnik2.koord_istochnik_GEOD.shirota;
    if(maxY<istochnik2.koord_istochnik_GEOD.shirota) maxY=1.5*istochnik2.koord_istochnik_GEOD.shirota;
    ui->graphicsView->yAxis->setRange(minY, maxY);//Для оси Oy
    ui->graphicsView->xAxis->setRange(minX, maxX);//Для оси Oy
    x3.push_back(istochnik2.koord_istochnik_GEOD.dolgota);
    y3.push_back(istochnik2.koord_istochnik_GEOD.shirota);
    ui->graphicsView->graph(7)->setData(x3, y3);
    ui->graphicsView->replot();
}

void MainWidget::on_Sbros_clicked()
{
 x.clear();
 y.clear();
 x1.clear();
 y1.clear();
 x2.clear();
 y2.clear();
 x3.clear();
 y3.clear();
 x4.clear();
 y4.clear();
 dann3.x.clear();
 dann4.x.clear();
 dann3.y.clear();
 dann4.y.clear();
 dann5.x.clear();
 dann6.x.clear();
 dann5.y.clear();
 dann6.y.clear();
 dann7.x.clear();
 dann8.x.clear();
 dann7.y.clear();
 dann8.y.clear();
 minY=0;maxY=0;minX=0;maxX=0;
 ui->graphicsView->clearGraphs();
 ui->graphicsView->replot();
}



void MainWidget::on_pushButton_clicked()
{
    QPen greenpen(Qt::cyan);
    ui->graphicsView->yAxis->setRange(minY, maxY);//Для оси Oy
    ui->graphicsView->xAxis->setRange(minX, maxX);//Для оси Oy
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(3)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(3)->setPen(greenpen);
    ui->graphicsView->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    ui->graphicsView->graph(3)->setData(dannx1.x,dannx1.y);

    ui->graphicsView->addGraph();
    ui->graphicsView->graph(4)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(4)->setPen(greenpen);
    ui->graphicsView->graph(4)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    ui->graphicsView->graph(4)->setData(dannx1.x1,dannx1.y1);

    ui->graphicsView->addGraph();
    ui->graphicsView->graph(5)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(5)->setPen(greenpen);
    ui->graphicsView->graph(5)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    ui->graphicsView->graph(5)->setData(dannx2.x,dannx2.y);


    ui->graphicsView->addGraph();
    ui->graphicsView->graph(6)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(6)->setPen(greenpen);
    ui->graphicsView->graph(6)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    ui->graphicsView->graph(6)->setData(dannx2.x1,dannx2.y1);
    ui->graphicsView->replot();
}

void MainWidget::on_pushButton_2_clicked()
{
   /* QPen greenpen(Qt::darkBlue);
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(5)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(5)->setPen(greenpen);
    ui->graphicsView->graph(5)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    QFile fileOut("fileout1.txt"); // Связываем объект с файлом fileout.txt
    if(fileOut.open(QIODevice::WriteOnly | QIODevice::Text))
        { // Если файл успешно открыт для записи в текстовом режиме
            //QTextStream writeStream(&fileOut); // Создаем объект класса QTextStream
    // и передаем ему адрес объекта fileOut
            // Посылаем строку в поток для записи
            //fileOut.close(); // Закрываем файл
        }
    QTextStream writeStream(&fileOut);
    double k=(maxX-minX)/100;
    writeStream<<k<<endl;
    double tic=0;
    double prom12=0,prom13=0;
     double ff1=0,ff2=0;
    for(int i=0;i<dann3.x.size();i++)
    {
        prom12 = gyperbol(tic,dann5);

        if(prom12>minY&&prom12<maxY)
        {
       dann7.y.push_back(prom12);
        dann7.x.push_back(tic);
        writeStream<<dann7.x[ff1]<<" "<<dann7.y[ff1]<<" "<<dann5.x[0]<<" "<<dann5.y[0]<<endl<<endl;
        ff1++;
        }

        prom13 = gyperbol(tic,dann6);
        if(prom13>minY&&prom13<maxY)
        {
        dann8.y.push_back(prom13);
        dann8.x.push_back(tic);
        writeStream<<dann8.x[ff2]<<" "<<dann8.y[ff2]<<" "<<tic<<" "<<dann6.x[0]<<" "<<dann6.y[0]<<endl<<endl;
        ff2++;
        }

        tic+=k;
    }

    ui->graphicsView->graph(5)->setData(dann7.x,dann7.y);
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(6)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(6)->setPen(greenpen);
    ui->graphicsView->graph(6)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,6));
    ui->graphicsView->graph(6)->setData(dann8.x,dann8.y);
    ui->graphicsView->replot();*/
}
